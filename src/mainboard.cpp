#include <Arduino.h> //required by all PlatformIO arduino projects
#include "IRremote.h" //IR receiver library
#include "DualVNH5019MotorShield.h" //motor shield library
#include <NewPingNo2.h> //Ultrasonic Sensor Library. No2 is a modification to avoid conflict with IRremote.h
#define forward 0xFF629D //define the IR hex values as commands. Used for remote control.
#define backward 0xFFA857 //move backwards
#define left 0xFF22DD //turn left on the spot
#define right 0xFFC23D //turn right on the spot
#define stop 0xFF02FD //stop movment current command
#define increase 0xFF906F //increase speed
#define decrease 0xFFE01F //decrease speed
#define change 0xFFE21D //changes between automatic and manual control
#define SONAR_NUM 2      // Number of ultrasonic sensors.
#define MAX_DISTANCE 500 // Maximum distance (in cm) to ping.

DualVNH5019MotorShield md; //Required by motor shield

NewPing sonar[SONAR_NUM] = {
    // Sensor object array.
    NewPing(2, 3, MAX_DISTANCE), //left sensor
    NewPing(12, 13, MAX_DISTANCE), //right sensor
    // Each sensor's trigger pin, echo pin, and max distance to ping.
};

int difference = 0; //the difference in outputs between the two sensors
int distanceL = 0;  //the reading of the left sensor (pins 2/3)
int distanceR = 0;  //the reading of the right sensor (pins 12/13)
int motorLspeed = 0; //the speed of the left motor (value from -400 to 400)
int motorRspeed = 0; //the speed of the right motor (value from -400 to 400)
int maxSpeed = 200; //the highest allowed value for motorLspeed and motorRspeed
int receiver = 11; //the data pin of the IR receiver
int lastTask = 0; //used for manual control, this represents the last command received by the sensor. 0-stop 1-forward 2-backward 3-left 4-right 5-increase 6-decrease
bool autocont = false;

IRrecv irrecv(receiver); // create instance of 'irrecv'
decode_results results;  // create instance of 'decode_results'

void setup()
{
  Serial.begin(9600);
  md.init(); //initalizes the motor shield
  Serial.println("IR Receiver Button Decode");
  irrecv.enableIRIn(); // Start the receiver
};

void translateIR(); // takes action based on IR code received

void loop()
{
  if (irrecv.decode(&results))
  {
    Serial.println(results.value, HEX);
    irrecv.resume(); // Receive the next value
    if (results.value == change)
    {
      autocont = !autocont;
    };
  };
  if (autocont == true)
  {

    for (uint8_t i = 0; i < SONAR_NUM; i++)
    {            // Loop through each sensor and display results.
      delay(35); // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.

      distanceL = (sonar[0].ping_median(3, 1000)); //stores the output of the left ultrasonic senor

      distanceR = (sonar[1].ping_median(3, 1000)); //stores the output of right ultrasonic sensor

      difference = distanceL - distanceR; //finds the difference of the two sensor outputs
      Serial.print(distanceL); //prints the microsecond response time for the left sensor
      Serial.print("  ms Left |");
      Serial.print(distanceR);
      Serial.print("  ms right |"); //prints the microsecond response time for the right sensor
      Serial.print(difference);
      Serial.print(" difference |"); //prints the left respnose time minus the right response time
      if (difference < 0)
      {
        Serial.print("Left");
      }
      else if (difference > 0)
      {
        Serial.print("right");
      }
      else
      {
        Serial.print("straight");
      }
      Serial.println();
    }
    md.setM1Speed(motorLspeed);
    md.setM2Speed(motorRspeed);

    if (abs(difference) != abs(distanceL) or abs(distanceR))
    { //Will not run if any sensor returns 0ms
      if (distanceL < distanceR)
      {
        //Serial.println("----RIGHT----");
        motorLspeed = (-100 + difference);
        /*
      Serial.print("|");
      Serial.print(motorLspeed);
      Serial.print("Left|");
      Serial.println();
      */
        motorRspeed = (-200 - motorLspeed);
        /*
      Serial.print("|");
      Serial.print(motorRspeed);
      Serial.print("Right|");
      Serial.println();
      */
      }
      else if (distanceL > distanceR)
      {
        //Serial.println("----LEFT----");
        motorLspeed = (-200 - motorRspeed);
        /*
      Serial.print("|");
      Serial.print(motorLspeed);
      Serial.print("Left|");
      Serial.println();
      */
        motorRspeed = (-100 - difference);
        /*
      Serial.print("|");
      Serial.print(motorRspeed);
      Serial.print("Right|");
      Serial.println();
      */
      }
      else if (distanceL == distanceR) //if senosors return same output go forwards
      {
        motorLspeed = -200;
        motorRspeed = -200;
      }
    }
    else if (abs(distanceL) + abs(distanceR) == 0) //if both sensors return zero stop
    {
      motorLspeed = (0);
      motorRspeed = (0);
    }
  };
  if (autocont == false) //manual remote control mode
  {

    if (irrecv.decode(&results))
    {
      Serial.println(results.value, HEX); //prints received IR Hex command
      irrecv.resume(); // Receive the next value

      switch (results.value) //defines and executes actions for each command
      {
      case stop:
        md.setM1Speed(0);
        md.setM2Speed(0);
        Serial.println("STOP");
        lastTask = 0;
        break;
      case forward:
        md.setM1Speed(maxSpeed);
        md.setM2Speed(maxSpeed);
        lastTask = 1;
        Serial.println("FORWARD");
        break;
      case backward:
        md.setM1Speed(-maxSpeed);
        md.setM2Speed(-maxSpeed);
        lastTask = 2;
        Serial.println("BACKWARD");
        break;
      case left:
        md.setM1Speed(-maxSpeed);
        md.setM2Speed(maxSpeed);
        lastTask = 3;
        Serial.println("LEFT");
        break;
      case right:
        md.setM1Speed(maxSpeed);
        md.setM2Speed(-maxSpeed);
        lastTask = 4;
        Serial.println("RIGHT");
        break;
      case increase:
        if (maxSpeed < 400)
        {
          maxSpeed = maxSpeed + 50;
        };
        lastTask = 5;
        Serial.println("INCREASE");
        break;
      case decrease:
        if (maxSpeed < 400)
        {
          maxSpeed = maxSpeed - 50;
        };
        lastTask = 6;
        Serial.println("DECREASE");
        break;
      }
    }
  };
}
