#include <Arduino.h>
#include <NewPing.h>

#define SONAR_NUM 2  // Number of sensors.
#define MAX_DISTANCE 500  // Maximum distance (in cm) to ping.


NewPing sonar[SONAR_NUM] = {     // Sensor object array.
  NewPing (12, 13, MAX_DISTANCE), // Each sensor's trigger pin, echo pin, and max distance to ping.
  NewPing (2, 3, MAX_DISTANCE),
};

void setup() {
  Serial.begin(9600);
}

void loop() {
  for (uint8_t i = 0; i < SONAR_NUM; i++) { // Loop through each sensor and display results.
    delay(35); // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    Serial.println(sonar[0].ping_cm());
    Serial.println(sonar[1].ping_cm());
}


}


